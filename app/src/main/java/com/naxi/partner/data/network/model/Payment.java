package com.naxi.partner.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payment {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("request_id")
    @Expose
    private Integer requestId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("provider_id")
    @Expose
    private Integer providerId;
    @SerializedName("fleet_id")
    @Expose
    private Integer fleetId;
    @SerializedName("promocode_id")
    @Expose
    private String promocodeId;
    @SerializedName("payment_id")
    @Expose
    private String paymentId;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("fixed")
    @Expose
    private Double fixed;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("minute")
    @Expose
    private Double minute;
    @SerializedName("hour")
    @Expose
    private Double hour;
    @SerializedName("commision")
    @Expose
    private Double commision;
    @SerializedName("commision_per")
    @Expose
    private Double commisionPer;
    @SerializedName("fleet")
    @Expose
    private Double fleet;
    @SerializedName("fleet_per")
    @Expose
    private Double fleetPer;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("discount_per")
    @Expose
    private Double discountPer;
    @SerializedName("tax")
    @Expose
    private Double tax;
    @SerializedName("tax_per")
    @Expose
    private Double taxPer;
    @SerializedName("wallet")
    @Expose
    private Double wallet;
    @SerializedName("is_partial")
    @Expose
    private Double isPartial;
    @SerializedName("cash")
    @Expose
    private Double cash;
    @SerializedName("card")
    @Expose
    private Double card;
    @SerializedName("surge")
    @Expose
    private Double surge;
    @SerializedName("tips")
    @Expose
    private Double tips;
    @SerializedName("total")
    @Expose
    private Double total;
    @SerializedName("payable")
    @Expose
    private Double payable;
    @SerializedName("provider_commission")
    @Expose
    private Double providerCommission;
    @SerializedName("provider_pay")
    @Expose
    private Double providerPay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRequestId() {
        return requestId;
    }

    public void setRequestId(Integer requestId) {
        this.requestId = requestId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getFleetId() {
        return fleetId;
    }

    public void setFleetId(Integer fleetId) {
        this.fleetId = fleetId;
    }

    public String getPromocodeId() {
        return promocodeId;
    }

    public void setPromocodeId(String promocodeId) {
        this.promocodeId = promocodeId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Double getFixed() {
        return fixed;
    }

    public void setFixed(Double fixed) {
        this.fixed = fixed;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Double getMinute() {
        return minute;
    }

    public void setMinute(Double minute) {
        this.minute = minute;
    }

    public Double getHour() {
        return hour;
    }

    public void setHour(Double hour) {
        this.hour = hour;
    }

    public Double getCommision() {
        return commision;
    }

    public void setCommision(Double commision) {
        this.commision = commision;
    }

    public Double getCommisionPer() {
        return commisionPer;
    }

    public void setCommisionPer(Double commisionPer) {
        this.commisionPer = commisionPer;
    }

    public Double getFleet() {
        return fleet;
    }

    public void setFleet(Double fleet) {
        this.fleet = fleet;
    }

    public Double getFleetPer() {
        return fleetPer;
    }

    public void setFleetPer(Double fleetPer) {
        this.fleetPer = fleetPer;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getDiscountPer() {
        return discountPer;
    }

    public void setDiscountPer(Double discountPer) {
        this.discountPer = discountPer;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTaxPer() {
        return taxPer;
    }

    public void setTaxPer(Double taxPer) {
        this.taxPer = taxPer;
    }

    public Double getWallet() {
        return wallet;
    }

    public void setWallet(Double wallet) {
        this.wallet = wallet;
    }

    public Double getIsPartial() {
        return isPartial;
    }

    public void setIsPartial(Double isPartial) {
        this.isPartial = isPartial;
    }

    public Double getCash() {
        return cash;
    }

    public void setCash(Double cash) {
        this.cash = cash;
    }

    public Double getCard() {
        return card;
    }

    public void setCard(Double card) {
        this.card = card;
    }

    public Double getSurge() {
        return surge;
    }

    public void setSurge(Double surge) {
        this.surge = surge;
    }

    public Double getTips() {
        return tips;
    }

    public void setTips(Double tips) {
        this.tips = tips;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getPayable() {
        return payable;
    }

    public void setPayable(Double payable) {
        this.payable = payable;
    }

    public Double getProviderCommission() {
        return providerCommission;
    }

    public void setProviderCommission(Double providerCommission) {
        this.providerCommission = providerCommission;
    }

    public Double getProviderPay() {
        return providerPay;
    }

    public void setProviderPay(Double providerPay) {
        this.providerPay = providerPay;
    }
}
