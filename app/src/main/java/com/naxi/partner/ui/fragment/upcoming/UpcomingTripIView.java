package com.naxi.partner.ui.fragment.upcoming;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
