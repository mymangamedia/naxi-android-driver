package com.naxi.partner.ui.activity.wallet;

import com.naxi.partner.base.MvpPresenter;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V> {

    void getWalletData();
}
