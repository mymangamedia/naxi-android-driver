package com.naxi.partner.ui.activity.setting;

import com.naxi.partner.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
