package com.naxi.partner.ui.activity.card;

import com.naxi.partner.base.MvpPresenter;

public interface CardIPresenter<V extends CardIView> extends MvpPresenter<V> {

    void deleteCard(String cardId);
    void card();
    void changeCard(String cardId);
}
