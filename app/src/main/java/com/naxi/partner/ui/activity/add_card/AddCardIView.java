package com.naxi.partner.ui.activity.add_card;

import com.naxi.partner.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);
    void onError(Throwable e);
}
