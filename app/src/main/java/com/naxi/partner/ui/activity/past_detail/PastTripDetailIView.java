package com.naxi.partner.ui.activity.past_detail;


import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
