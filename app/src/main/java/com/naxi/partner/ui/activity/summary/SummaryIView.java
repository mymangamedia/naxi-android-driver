package com.naxi.partner.ui.activity.summary;


import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);
    void onError(Throwable e);
}
