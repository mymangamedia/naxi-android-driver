package com.naxi.partner.ui.activity.change_password;

import com.naxi.partner.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
