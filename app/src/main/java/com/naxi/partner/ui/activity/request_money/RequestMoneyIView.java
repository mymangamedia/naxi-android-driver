package com.naxi.partner.ui.activity.request_money;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
