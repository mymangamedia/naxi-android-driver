package com.naxi.partner.ui.activity.profile;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);
    void onSuccessUpdate(UserResponse object);
    void onError(Throwable e);

}
