package com.naxi.partner.ui.bottomsheetdialog.cancel;

import com.naxi.partner.base.MvpView;

public interface CancelDialogIView extends MvpView {

    void onSuccessCancel(Object object);
    void onError(Throwable e);
}
