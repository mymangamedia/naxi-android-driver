package com.naxi.partner.ui.fragment.past;


import com.naxi.partner.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
