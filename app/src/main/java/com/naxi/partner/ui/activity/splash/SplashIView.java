package com.naxi.partner.ui.activity.splash;

import com.naxi.partner.base.MvpView;

public interface SplashIView extends MvpView {


    void redirectHome();

    void onSuccess(Object user);
    void onError(Throwable e);
}
