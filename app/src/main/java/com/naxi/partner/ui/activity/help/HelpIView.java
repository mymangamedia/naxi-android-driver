package com.naxi.partner.ui.activity.help;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);
    void onError(Throwable e);
}
