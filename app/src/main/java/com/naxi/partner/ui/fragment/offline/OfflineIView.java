package com.naxi.partner.ui.fragment.offline;

import com.naxi.partner.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
