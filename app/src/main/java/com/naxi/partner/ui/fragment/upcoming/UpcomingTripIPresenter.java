package com.naxi.partner.ui.fragment.upcoming;


import com.naxi.partner.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
