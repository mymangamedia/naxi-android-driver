package com.naxi.partner.ui.activity.regsiter;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.User;

public interface RegisterIView extends MvpView {

    void onSuccess(User user);
    void onSuccess(Object verifyEmail);

    void onError(Throwable e);

}
