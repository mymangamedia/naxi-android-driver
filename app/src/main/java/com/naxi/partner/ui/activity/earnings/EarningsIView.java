package com.naxi.partner.ui.activity.earnings;


import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);
    void onError(Throwable e);
}
