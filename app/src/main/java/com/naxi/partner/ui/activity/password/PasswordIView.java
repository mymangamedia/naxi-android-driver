package com.naxi.partner.ui.activity.password;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.ForgotResponse;
import com.naxi.partner.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onSuccess(User object);
    void onError(Throwable e);
}
