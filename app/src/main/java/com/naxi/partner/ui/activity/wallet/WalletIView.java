package com.naxi.partner.ui.activity.wallet;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);
    void onError(Throwable e);
}
