package com.naxi.partner.ui.activity.summary;


import com.naxi.partner.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary();
}
