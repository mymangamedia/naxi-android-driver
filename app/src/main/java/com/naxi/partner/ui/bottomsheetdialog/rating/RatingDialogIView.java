package com.naxi.partner.ui.bottomsheetdialog.rating;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
