package com.naxi.partner.ui.fragment.past;


import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
