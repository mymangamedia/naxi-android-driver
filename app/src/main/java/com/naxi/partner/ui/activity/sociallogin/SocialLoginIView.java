package com.naxi.partner.ui.activity.sociallogin;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
