package com.naxi.partner.ui.bottomsheetdialog.invoice_flow;

import com.naxi.partner.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
