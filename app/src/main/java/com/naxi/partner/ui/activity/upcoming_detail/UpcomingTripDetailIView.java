package com.naxi.partner.ui.activity.upcoming_detail;


import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
