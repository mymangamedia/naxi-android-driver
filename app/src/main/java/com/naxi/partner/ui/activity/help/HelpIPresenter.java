package com.naxi.partner.ui.activity.help;


import com.naxi.partner.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
