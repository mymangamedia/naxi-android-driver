package com.naxi.partner.ui.fragment.status_flow;

import com.naxi.partner.base.MvpView;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
