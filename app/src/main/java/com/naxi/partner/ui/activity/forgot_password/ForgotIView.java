package com.naxi.partner.ui.activity.forgot_password;

import com.naxi.partner.base.MvpView;
import com.naxi.partner.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
