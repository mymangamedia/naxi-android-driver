package com.naxi.partner.ui.activity.earnings;


import com.naxi.partner.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
