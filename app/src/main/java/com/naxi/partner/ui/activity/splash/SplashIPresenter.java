package com.naxi.partner.ui.activity.splash;

import com.naxi.partner.base.MvpPresenter;

public interface SplashIPresenter<V extends SplashIView> extends MvpPresenter<V> {

    void handlerCall();

    void getPlaces();

}
